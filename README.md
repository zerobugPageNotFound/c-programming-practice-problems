LIST OF PROGRAMS :

    Hello World!
    Area and circumference
    Basic Arithmatic
    Fahrenheit To Celcius
    Lowercase To Uppercase
    Uppercase To Lowercase
    Simple Interest Calculator
    Leap Year Using Ternary Operator
    Sizeof Operator
    Find ASCII Value
    Bitwise Left Shift Operator
    Bitwise Complement Operator
    Bitwise AND Operator
    Bitwise Odd or Even
    Increment Operator
    Decrement Operator
    Nested If Leap Year
    Nested if Greatest Integer
    Check Character Type
    Employee Grade
    Daily Wage Calc
    Day Name Using Switch Case
    Vowel or Consonant
    Calculator using switch case
    Goto statement
    Mirror Number
    Dynamic 2D Array Using One Pointer
    Dyanamic 2D Array using Array of Pointer
    Digital Root of a Number
    Swap By Reference Vs Swap By Copy
    Display Linux Environment Variables
    Factorial
    Get String Length
    Pointers in C
    Binary Search
    Recursion
    Segmentation Fault or Bus Error Demo
    Structure
    Swapping 2 Numbers Without a Third Variable or ^
    Print 100 Prime numbers using Seive of Eratosthenes
    Palindrome Number
    Temperature conversion
    Alphabet triangle
    Armstrong Number
    Celsius to Kelvin
    Calculator
    Common elements in two array
    Decimal to base n conversion
    Two-way decimal to hexadecimal
    Calculate remainder
    Check leap year
    Find largest number
    Check odd or even
    Print pattern
    Polynomial linklist
    Print prime numbers
    Factorial of a number with recursion
    Simple EMI Calculator
    Simple multiplication table
    Square root
    Stack implemenation of linklist
    Perform Selection Sort
    Calculate Factorial upto input of 5000
    To calculate the Least Common Multiple
