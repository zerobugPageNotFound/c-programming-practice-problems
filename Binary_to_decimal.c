/* Program to convert binary number to decimal */
/* Neglet the warnings if any */
#include<stdio.h>
#include<math.h>
int main(){
	unsigned long long int a;
	printf("Enter the number in binary:");
	scanf("%llu",&a);
	int b=0,j=0;
	int c;
	while(a!=0){
		c=a%10;
		b=b+c*pow(2,j);
		j++;
		a=a/10;
	}
	printf("%llu",b);
	return 0;
}
