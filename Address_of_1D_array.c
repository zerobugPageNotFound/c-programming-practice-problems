/* Program to print the addresses of 1-D array. */
#include<stdio.h>
int main(){
	int a[100],i,n,*add;
	printf("Enter the size :");
	scanf("%d",&n);
	printf("Enter the numbers :");
	for(i=0;i<n;i++) {
		scanf("%d",&a[i]);
	}
	for(i=0;i<n;i++) {
		add=(a+(i*sizeof(int)));
		printf("%u\n",add);
	}
	return 0;
}
