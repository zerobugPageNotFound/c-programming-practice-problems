/* Program for simple calculator using switch case */
#include <stdio.h>
int main(){
    int a, b, c;
    int ch;
    printf("Enter two number: ");
    scanf("%d%d", &a, &b);
    printf("Enter your choice: \n");
    printf("1. Enter 1 for addition\n");
    printf("2. Enter 2 for subtraction\n");
    printf("3. Enter 3 for multiplication\n");
    printf("4. Enter 4 for division\n");
    printf("5. Enter 5 for modulo division\n");
    scanf("%d", &ch);
    switch (ch){
    case 1:
      c = a + b;
      break;
    case 2:
      c = a - b;
      break;
    case 3:
      c = a * b;
      break;
    case 4:
      c = a / b;
      break;
    case 5:
      c = a % b; //Modulus division only works with integers.
      break;
    default:
      printf("wrong choice");
    }
    printf("calculated value=%d", c);
    return 0;
}
